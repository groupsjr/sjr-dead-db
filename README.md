# SJR Dead DB

This plugin adds the db-error.php drop-in file to `wp-content`, and creates static copies of pages to be served when the database fails.

Copies of the served HTML are generated every hour, when a page is visited, and stored in the directory which can be defined in the constant `SJR_DEAD_DB_CACHE_DIR`, which defaults to `/cached/` within the plugin directory.  

The caching takes URL query parameters into account, and will serve first a cache with the exact query match, and attempt to serve one with fewer matching query parameters if possible.

https://developer.wordpress.org/reference/functions/dead_db/