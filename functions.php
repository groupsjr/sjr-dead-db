<?php 

namespace sjr\dead_db;


/**
*	gets the absolute file path to the cached file for the request
*	@param string
*	@param array
*	@return string
*/
function get_page_path( $request_uri, $request_params = array() ){
	$dir = str_replace( '//', '/', SJR_DEAD_DB_CACHE_DIR.$request_uri );
	$file = str_replace( '//', '/', $dir.'/index' );

	if( !empty($request_params) )
		$file .= '.'.md5( http_build_query($request_params) );
	
	$file .= '.php';

	return $file;
}

/**
*	render a page into wherever
*	(only used in admin screen)
*	@param string
*	@param object|array
*	@return string
*/
function render( $filename, $vars = array() ){
	$template = __DIR__.'/views/'.$filename.'.php';
	ob_start();
	if( file_exists($template) ){
		extract( (array) $vars, EXTR_SKIP );
		include $template;
	}

	$html = ob_get_clean();

	return $html;
}

/**
*	generates the transient key for the request
*	@param string
*	@param array
*	@return string
*/
function transient_key( $request_uri, $request_params ){
	$key = substr( 'static-'.md5(version().$request_uri.serialize($request_params)), 0, 40 );
	return $key;
}

/**
*
*	@param string
*	@return string
*/
function url( $path ){
	$data = get_file_data( $path, array('URL' => 'URL'), 'plugin' );
	
	return $data['URL'];
}

/**
*	gets current version of plugin
*	@return string
*/
function version(){
	static $data;

	if( !$data )
		$data = get_file_data( __DIR__.'/_plugin.php', array('Version' => 'Version'), 'plugin' );
	
	return $data['Version'];
}