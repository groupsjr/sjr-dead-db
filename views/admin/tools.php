<div class="wrap">
	<h2>Dead DB</h2>
	<pre><?php echo $version; ?></pre>

	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row">
					<label>Cache Directory</label>
				</th>
				<td>
					<input type="text" value="<?php echo $cache_dir; ?>" class="code regular-text"/>

					<p>
						This can be changed by defining a constant <span class="code">SJR_DEAD_DB_CACHE_DIR</span> in the config.
					</p>

					<?php if( !empty($cache_dir_error) ): ?>
					<p class="error">
						<?php echo $cache_dir_error; ?>
					</p>
					<?php endif; ?>
				</td>
			</tr>

			<tr>
				<th scope="row">
					<label>DB File</label>
				</th>
				<td>
					<input type="text" value="<?php echo $db_file; ?>" class="code regular-text" disabled/>

					<?php if( !empty($db_file_error) ): ?>
					<p class="error">
						<?php echo $db_file_error; ?>
					</p>
					<?php endif; ?>
				</td>
			</tr>
		</tbody>
	</table>


	<h3>Cached files</h3>

	<ul>
	<?php foreach( $files as $file ): ?>
		<li>
			<?php echo $file->url; ?>
			<a title="Delete" href="<?php echo $file->url_delete; ?>"><i class="fa fa-times"></i></a>
			<a title="View" href="<?php echo $file->url_view; ?>" target="blank"><i class="fa fa-eye"></i></a>
		</li>
	<?php endforeach; ?>
	</ul>

	<form action="<?php echo admin_url('tools.php?page=sjr-dead-db'); ?>" method="post">
		<button type="submit" name="sjr-dead-db-clear" value="<?php echo wp_create_nonce( 'clear-cache-all' ); ?>">Clear all cache files</button>
	</form>
</div>