<?php echo "<?php \n"; ?>
/*
*	File: <?php echo $file; ?> 
*	URL: <?php echo $request_url; ?> 
*/

http_response_code( <?php echo $code; ?> );

<?php foreach( $headers as $key => $header ): ?>
header( '<?php echo $key; ?>: <?php echo $header; ?>' ); 
<?php endforeach; ?>

<?php 

echo '$html = <<<', $heredoc, "\n";
echo $body, "\n";
echo $heredoc, ';', "\n"; 
echo 'echo $html;';