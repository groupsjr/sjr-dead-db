<?php
/**
Plugin Name: 	SJR Dead DB
Plugin URI: 
Description:	
Author:			Group SJR | Eric Eaglstun
Version:		0.7.9
Author URI: 

This file must be parsable by php 5.2
*/

register_activation_hook( __FILE__, create_function("", '$ver = "5.3"; if( version_compare(phpversion(), $ver, "<") ) die( "This plugin requires PHP version $ver or greater be installed." );') );
register_activation_hook( __FILE__, 'sjr\dead_db\copy_db_file' );
register_deactivation_hook( __FILE__, 'sjr\dead_db\remove_db_file' );

require __DIR__.'/index.php';