<?php 

namespace sjr\dead_db;

require __DIR__.'/functions.php';
if( is_admin() )
	require __DIR__.'/admin.php';

// @todo this is not used in db-error.php may not be possible to have a custom path if the db fails.
if( !defined('SJR_DEAD_DB_CACHE_DIR') )
	define('SJR_DEAD_DB_CACHE_DIR', __DIR__.'/cached/' );

define('SJR_DEAD_DB_FILE', WP_CONTENT_DIR . '/db-error.php' );

/**
*	run on activation
*	@return bool
*/
function copy_db_file(){
	return copy( __DIR__.'/db-error.php', SJR_DEAD_DB_FILE );
}

/**
*	run on deactivation
*	@return
*/
function remove_db_file(){
	if( file_exists(SJR_DEAD_DB_FILE) )
		unlink( SJR_DEAD_DB_FILE );

	// @todo recursively delete cache directory
	// @todo delete transients
}

/**
*	checks if the page has been cached in the last hour, and sets up request on shutdown to recache
*	
*/
function plugins_loaded(){
	// sjr core not activated, show notice in admin
	if( !function_exists('\sjr\version') ){
		add_action( 'admin_notices', __NAMESPACE__.'\admin_notice_no_core' );
		return;
	}

	// force viewing cached url, used from links generated in admin
	if( isset($_GET['use-cache']) && isset($_GET['_wp_nonce']) ){
		$nonce = $_GET['_wp_nonce'];
		$url = \sjr\parse_url( site_url($_SERVER['REQUEST_URI']) );

		unset( $url['query']['_wp_nonce'] );
		unset( $url['query']['use-cache'] );

		$file = get_page_path( $url['path'], $url['query'] );
		$url = \sjr\unparse_url( $url );

		if( wp_verify_nonce($nonce, 'use-cache-single-'.$url) && file_exists($file) ){
			include $file;
			die();
		}
	}

	if( is_admin() && (!defined('DOING_AJAX') || !DOING_AJAX) ){
		return;
	} elseif( isset($_POST['cache_request']) ){
		return;
	} else {
		$request_uri = \sjr\parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH );
		$request_params = \sjr\parse_url( $_SERVER['REQUEST_URI'], PHP_URL_QUERY );

		$transient_key = transient_key( $request_uri, $request_params );
		$cached = get_transient( $transient_key );

		$file = get_page_path( $request_uri, $request_params );

		// for testing
		//return send_cache_request( $request_uri, $request_params );

		if( $cached === FALSE || !file_exists($file) ){
			register_shutdown_function( function() use($request_uri, $request_params){
				send_cache_request( $request_uri, $request_params );
			} );
		}	
	}
}
add_action( 'plugins_loaded', __NAMESPACE__.'\plugins_loaded' );

/**
*	sends the request to the server, and stores response as html file
*	@param string
*	@param array
*	@return void
*/
function send_cache_request( $request_uri = '', $request_params = array() ){
	$request_url = site_url( $request_uri );

	if( !empty($request_params) )
		$request_url .= '?'.http_build_query( $request_params );

	$response = wp_remote_post( $request_url, array(
		'body' => array(
			'cache_request' => 1
		),
		'timeout' => 60,
	) );

	if( is_wp_error($response) )
		return;

	$code = wp_remote_retrieve_response_code( $response );
	if( $code >= 400 ) {
		remember_error_page($code, $request_uri, $request_params);
		return;
	}

	// html body
	$body = wp_remote_retrieve_body( $response );


	// response headers
	$headers = wp_remote_retrieve_headers( $response );
	$headers['sjr'] = 'dead db';
	unset( $headers['set-cookie'] );

	// heredoc to escape html echoing
	$heredoc = 'SJRDEADDB_';
	do {
		$heredoc .= strtoupper( wp_generate_password(4, FALSE, FALSE) );
	} while( strpos($body, $heredoc) !== FALSE );

	$file = get_page_path( $request_uri, $request_params );
	$dir = dirname( $file );

	$rendered = render( 'index', array(
		'body' => $body,
		'code' => $code,
		'file' => $file,
		'headers' => $headers,
		'heredoc' => $heredoc,
		'request_url' => $request_url
 	) );

	if( !file_exists($dir) )
		wp_mkdir_p( $dir );

	file_put_contents( $file, $rendered );

	$transient_key = transient_key( $request_uri, $request_params );
	set_transient( $transient_key, $request_url, HOUR_IN_SECONDS );
}

/**
*
*	@param
*	@param
*	@param
*	@return
*/
function remember_error_page( $code, $target, $params=array() ){
	error_log( "Adding hour-log exception for $target ($code)" );
	$transient_key = transient_key( $target, $target );
	set_transient( $transient_key, $target, HOUR_IN_SECONDS );
}
