<?php

namespace sjr\dead_db;

/**
*
*/
function admin_menu(){
	add_management_page( 
		'Dead DB',  
		'Dead DB', 
		'manage_options', 
		'sjr-dead-db', 
		__NAMESPACE__.'\tools' 
	);
}
add_action( 'admin_menu', __NAMESPACE__.'\admin_menu' );

/**
*
*/
function admin_save(){
	// delete single cache file
	$single_file = isset( $_GET['delete-file'] ) ? $_GET['delete-file'] : FALSE; 
	if( $single_file && wp_verify_nonce($_GET['_wp_nonce'], 'clear-cache-single-'.$single_file ) ){
		$ok = file_exists( $single_file ) && unlink( $single_file );
		// @TODO add message to ui
	}

	// delete entire cache
	$clear_all = isset( $_POST['sjr-dead-db-clear'] ) ? wp_verify_nonce( $_POST['sjr-dead-db-clear'], 'clear-cache-all' ) : FALSE;
	if( $clear_all ){
		require_once ABSPATH.'wp-admin/includes/class-wp-filesystem-base.php';
		require_once ABSPATH.'wp-admin/includes/class-wp-filesystem-direct.php';

		$fs = new \WP_Filesystem_Direct( '' );
		$fs->delete( SJR_DEAD_DB_CACHE_DIR, TRUE );
	}
}
add_action( 'load-tools_page_sjr-dead-db', __NAMESPACE__.'\admin_save' );

/**
*	show notice if SJR Core is not active
*/
function admin_notice_no_core(){
	echo '<div class="error">SJR Core is required for Dead DB</div>';
}

/**
*	render tools admin page
*/
function tools(){
	wp_enqueue_style( 'sjr-dead-db-tools', plugins_url( 'public/admin/tools.css', __FILE__ ), 
                       array(), '' );

	wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', 
                       array(), '' );

	$vars = array(
		'cache_dir' => esc_attr(SJR_DEAD_DB_CACHE_DIR),
		'db_file' => esc_attr(SJR_DEAD_DB_FILE),
		'files' => array(),
		'version' => version()
	);

	// errors
	if( !file_exists(SJR_DEAD_DB_CACHE_DIR) && !wp_mkdir_p(SJR_DEAD_DB_CACHE_DIR) )
		$vars['cache_dir_error'] = 'Cache directory does not exist and could not be created';
	elseif( !file_exists(SJR_DEAD_DB_CACHE_DIR) )
		$vars['cache_dir_error'] = 'Cache directory does not exist';
	elseif( !is_writeable(SJR_DEAD_DB_CACHE_DIR) )
		$vars['cache_dir_error'] = 'Cache directory is not writeable';
	elseif( !is_readable(SJR_DEAD_DB_CACHE_DIR) )
		$vars['cache_dir_error'] = 'Cache directory is not readable';
	
	if( !file_exists(SJR_DEAD_DB_FILE) && !copy_db_file() )
		$vars['db_file_error'] = 'db-error.php does not exist and could not be created';
	elseif( !file_exists(SJR_DEAD_DB_FILE) )
		$vars['db_file_error'] = 'db-error.php does not exist';

	// cached files
	$dir = new \RecursiveDirectoryIterator( SJR_DEAD_DB_CACHE_DIR );
	$iterator = new \RecursiveIteratorIterator( $dir );
	foreach( $iterator as $file ){
		if( !$file->isDir() ){
			$path = $file->getRealPath();
			$url = url( $file->getRealPath() );

			$url_delete = add_query_arg( array(
				'delete-file' => $path,
				'_wp_nonce' => wp_create_nonce( 'clear-cache-single-'.$path )
			) );

			$url_view = add_query_arg( array(
				'use-cache' => 1,
				'_wp_nonce' => wp_create_nonce( 'use-cache-single-'.$url )
			), $url );

			array_push( $vars['files'], (object) array(
				'path' => $path,
				'url' => $url,
				'url_delete' => $url_delete,
				'url_view' => $url_view
			) );
		}
	}

	echo render( 'admin/tools', $vars );
}